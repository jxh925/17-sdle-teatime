from setuptools import setup

setup(name='mathPlot',
      version='0.0.1',
      description='does some cool stuff',
      url='http://engineering.case.edu/centers/sdle/',
      author='author name',
      author_email='author@email',
      license='GPL3',
      packages=['mathPlot'],
      package_dir={'mathPlot': './mathPlot'},
      package_data={'mathPlot': ['files/data/*','files/docs/*','README.rst']},
      install_requires=['markdown'],
      include_package_data=True,
      zip_safe=False)
