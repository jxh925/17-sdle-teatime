def rstdout(a1,a2,a3):
    """This function adds two numbers together.

    Args:
        arg1 (numeric): The first parameter.
        arg2 (numeric): The second parameter.

    Returns:
        res (numeric): The result of adding the two input parameters together.
    """
    import subprocess
    import os
    this_dir, this_filename = os.path.split(__file__)
    path2script = os.path.join(this_dir, "files/data/rstdo.R")
    args = [str(a1),str(a2),str(a3)]
    cmd = ['Rscript', path2script] + args
    return(subprocess.check_output(cmd, universal_newlines=True)[0])
