from setuptools import setup

setup(name='pywithr',
      version='0.0.1',
      description='does some cool stuff',
      url='http://engineering.case.edu/centers/sdle/',
      author='author name',
      author_email='author@email',
      license='GPL3',
      packages=['pywithr'],
      package_dir={'pywithr': './pywithr'},
      package_data={'pywithr': ['files/data/*','files/docs/*','README.rst']},
      install_requires=['markdown'],
      include_package_data=True,
      zip_safe=False)
