---
title: "EDA_Tutorial"
author: "Amit K Verma"
date: "July 3, 2017"
output: pdf_document
---
##  Exploratory Data Analysis Steps

### Step 1: Formulate your question. 

- Useful way to guide the analysis.
- To limit the exponential number of paths that can be taken with any sizeable dataset.

### Step 2: Reading dataset

- Hourly CO levels in the United States for the year 2017 (first four months)
- An air pollution dataset from the U.S. Environmental Protection Agency (EPA) - EPA’s Air Quality System web page

Questions can be refined at any point.

- Are air pollution levels higher on the east coast than on the west coast?
- Are hourly CO levels on average higher in New York City than they are in Los Angeles?
- Which counties in the United States have the highest levels of ambient CO pollution?

```{r}
CO <- read.csv("hourly_42101_2017.csv", header = TRUE, stringsAsFactors=FALSE)
```

#### Check the packaging.

- If you are using rstudio then you can directly read from the environment.
- Identify potential problems with the data before plunging in head first into a complicated data analysis.

```{r}
nrow(CO)
# 326343
ncol(CO)
# 24
str(CO)
```

#### Check your “n”s

- "n" stands for number 
- if you’re expecting there to be 1,000 observations and it turns out there’s only 20, you know something must have gone wrong somewhere.
- Hourly Data: The monitors should be monitoring continuously during the day, so all hours should be represented. 

```{r}
table(CO$Time.Local)
# This looks reasonable !

library(dplyr)
# Since EPA monitors pollution across the country, there should be a good representation of states. 
select(CO, State.Name) %>% unique %>% nrow
# 13 states are missing

# We can check !!
unique(CO$State.Name)
```

### Step 3: Validate with at least one external data source

- Making sure your data matches something outside of the dataset is very important.
- National ambient air quality standards
- CO Emissions
    - 8 hours 9 ppm; 
    - 1 hour 35 ppm; 
    - Not to be exceeded more than once per year
- https://www.epa.gov/criteria-air-pollutants/naaqs-table

```{r}
summary(CO$Sample.Measurement)
quantile(CO$Sample.Measurement, seq(0, 1, 0.1))
```

- The data are at least of the right order of magnitude (i.e. the units are correct)
- The range of the distribution is roughly what we’d expect, given the regulation around ambient pollution levels
- Some hourly levels (less than 5%) are close to 9 ppm but this may be reasonable given the wording of the standard and the averaging involved.

### Step 4: Try the easy solution first

- Question: Which counties in the United States have the highest levels of ambient CO pollution?
- How could you provide prima facie evidence for your hypothesis or question.
- For now, let’s just take the average across the entire year for each county and then rank counties according to this metric.
- To identify each county we will use a combination of the State.Name and the County.Name variables.

```{r}
ranking <- group_by(CO, State.Name, County.Name) %>% summarize(CO = mean(Sample.Measurement)) %>% 
  as.data.frame %>% arrange(desc(CO))

# Top 10 counties in this ranking
head(ranking, 10)
# Interesting that 6 of these counties are in the western U.S., with 4 of them in California alone.

# We can look at the 10 lowest counties too.
tail(ranking, 10)

# Let’s take a look at one of the higest level counties, Louisiana, Orleans
filter(CO, State.Name == "Louisiana" & County.Name == "Orleans") %>% nrow
```

- 365 days = 8760 hrs
- Looks like 4 months of data
- How CO varies through the year in this county by looking at monthly averages

```{r}
# we’ll need to convert the date variable into a Date class.
CO <- mutate(CO, Date.Local = as.Date(Date.Local))

# we'll split the data by month to look at the average hourly levels.
filter(CO, State.Name == "Louisiana" & County.Name == "Orleans") %>%
           mutate(month = factor(months(Date.Local), levels = month.name)) %>%
           group_by(month) %>%
           summarize(CO = mean(Sample.Measurement))
```

- Something happened in Febuary which led to 40% increase in CO emissions
- It’s not immediately clear why that is, but it’s probably worth investigating a bit later on.

- How stable are the rankings from year to year? 
- We only have one year’s worth of data for the moment, 
- But we could perhaps get a sense of the stability of the rankings by shuffling the data around to see if anything changes.

### Step 5: Follow up questions
 
- Do you have the right data?
- Do you need other data? 
- Do you have the right question? 

## Principles of Analytic Graphics

- Inspired by Edward Tufte’s wonderful book **Beautiful Evidence**
- How to make informative and useful data graphics and lays out six principles that are important to achieving that goal.

## Exploratory Graphs

- The goal of making exploratory graphs is usually developing a personal understanding of the data and to prioritize tasks for follow up.

- New Question: Are there any counties in the U.S. that exceed the national standard for CO emissions?

### One Dimension Data

- Five number summary : minimum, 25th percentile, median, 75th percentile, maximum of the data 

```{r}
fivenum(CO$Sample.Measurement)
summary(CO$Sample.Measurement)
```

- Boxplots: visual representation of the five-number summary
    - the “whiskers” that stick out above and below the box have a length of 1.5 times the inter-quartile range (or IQR), 
    - which is simply the distance from the bottom of the box to the top of the box. 
    - Anything beyond the whiskers is marked as an “outlier” and is plotted separately as an individual point.

```{r}
boxplot(CO$Sample.Measurement, col = "blue")

library(dplyr)
temp <- filter(CO, Sample.Measurement > 2)
unique(temp$State.Name)
```

- Barplot: for visualizing categorical data 

```{r}
table(CO$State.Name[CO$Sample.Measurement > 2]) %>% barplot(col = "wheat", main="CO Emissions",ylab="Freqency",xlab="",las=2)

library(maps)
map("county", "Arizona")
with(filter(CO, Sample.Measurement > 2), points(Longitude, Latitude))
```

- Histograms show the complete empirical distribution of the data, beyond the five data points shown by the boxplots.
    - You can easily check skewwness of the data, symmetry, multi-modality, and other features. 

```{r}
hist(CO$Sample.Measurement, col = "green", breaks = 50)
rug(CO$Sample.Measurement)
```

- Density plot: The density() function computes a non-parametric estimate of the distribution of a variables.

### Two Dimensions and Beyond

- Multiple or overlayed 1-D plots 
- Scatterplots - Variable Transformation

- Overlayed or multiple 2-D plots; conditioning plots (coplots)
- Use color, size, shape to add dimensions
- Spinning/interactive plots
