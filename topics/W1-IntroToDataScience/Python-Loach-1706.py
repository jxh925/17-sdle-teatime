# -*- coding: utf-8 -*-
"""
Created on Wed May 31 09:18:30 2017

Teatime script for exploring python and spyder
Note: The code in this file is intended to be used in an IPython teminal. It is
not executable.

@author: Andrew Loach
@License: Creative Commons Attribution-NonCommercial-ShareAlike- 4.0
"""
# =============================================================================
# Python Language
#
# Python is a high level language that is very similar to R. Both are open
# source high-level interpreted programming languages that use dynamic typing.
# Both have a number of packages and libraries developed for a wide number of
# purposes including Data Science.
#
# It is recommended to use the Anaconda distribution, which includes almost all
# of the widely used python tools and packages. Most notably it includes the
# conda and pip package managers as well as ipython, jupyter, matplotlib,
# numpy, pandas, python 2.7 (or 3.6), scikit-image, scikit-learn, scipy,
# and spyder.
#
# https://www.continuum.io/downloads
#
# This IDE is called spyder, it is very similar to Rstudio. It has an
# interactive Python (IPython) console, which makes python easier to use by
# adding many things such as syntax highlighting, inline figures, tab
# completion, shell access, and easy access to documentation. The spyder
# tutorial is well written and can answer your questions or help you get
# started.
#
# Initialization
#
# It is good style to start scripts and documents by initializing the
# environment and loading the packages and modules that you will use later.
# =============================================================================

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
%matplotlib inline

# =============================================================================
# Object types and Variable Assignment
#
# Let's get familiar with some of the most common object types in python.
# =============================================================================

type?

# ints and floats
a = 5
b = 5.
c = 3.14
d = 2e3
e = np.pi

# bool
f = 1 != 2
g = 1 < 2
h = (1 == 2 and 1 < 2) or True

# string
s = "m"
s = "this is a string"

# Lists, tuples and arrays
x = [1, 2, 3, 4, 5]
x = range(1, 6)
y = (a, b, c)
z = np.array(x)
z = np.arange(1, 6)

# If time: multidimensional arrays

# =============================================================================
# Attributes
#
# Each object has a number of attributes that belong to the class or the
# instance of the object that can be called. Similarly, modules have functions
# and classes that belong to them just as they belong to packages. Calling
# attributes is done using a "." Whatever is to the right of the dot belongs
# to the item on the left.
# =============================================================================

dir?
dir(s)
s = s.upper()

# If time: operators and copying iterable objects

# =============================================================================
# Indexing
#
# Indices in python always start at zero. You may also use negative numbers to
# count indices from the back. The [:] is a slicing operator that is very
# useful and much faster than loops, but we will not go into detail for this
# presentation.
#
# Printing
#
# The print statement (a function in python 3) is used for displaying something
# or multiple things to the standard output.
# =============================================================================
print x[1]
print x[-1]
print x[0:3]

# If time: slicing with steps and indexing for multidimensional arrays

# =============================================================================
# Statements: If, Loops, and functions
#
# Python uses colons and tab whitespace to define the blocks of code in which
# statements operate upon. Examples are shown below.
#
# Keep in mind that like R, variables that are created in functions do not
# exist outside of them. Keep in mind that array slicing and built in functions
# are typically much faster than loops and should be used when possible.
# =============================================================================

# if statements
if a != c:
    print "x =", x, "\ny =", y
else:
    print "x and y are equal to:", x

# loops
stringList = ("a", "b", "c", "z")

for letter in stringList:
    print letter

for i in range(len(stringList)):
    print "The letter", stringList[i]
    print "is at index", i

# functions
def basic():
    x = "text"
    return(x)


def sumsquares(x):
    sumx = 0
    for i in range(len(x)):
        sumx = sumx + x[i]**2
    return(sumx)

# If time: optional arguments

# =============================================================================
# Plotting
#
# Python has a very nice plotting package that allows you to make anything from
# a quick plot to explore your data to something aesthetically pleasing and
# ready for a presentation. Alternatively you can also import ggplot. A couple
# examples are shown below.
# =============================================================================

# make the data
x = np.linspace(-2, 2, 100)
y = x**2

# A quick plot to visualize
plt.plot(x, y)

# Data for next plot
x = np.linspace(.2, 6, 30)

# A more advanced plot with many extra features
matplotlib.rc('font', size=15)

fig = plt.figure(figsize=(9, 6))
plt.plot(x, x, 'ro', label="$y = x$")
plt.plot(x, 2**x, 'gs', label="$y = 2^x$")
plt.plot(x, x**2, 'b^', label="$y = x^2$")
plt.xlabel('X-values')
plt.ylabel('Y-values (log scale)')
plt.yscale('log')
plt.title('Example Plot With Many Extra Features')
plt.legend(loc="best")
plt.minorticks_on()
plt.grid(True, which='major', linewidth=1)
plt.grid(which='minor', linewidth=.25)

# If time: Basic File I/O

# =============================================================================
# Demo
#
# Python has a large number of packages developed for almost every field with
# computational applications. Signal analysis is jus one of those. For this
# demo we will create a noisy signal and then extract the dominant frequencies.
# =============================================================================


def signal_funct(x):
    y = .5 * np.sin(x) + .5 * np.sin(x/2) + .8 * np.sin(2*x)
    return(y)

# Create our data
numPoints = 1001
time = np.linspace(0, 12*np.pi, numPoints)
amplitude = signal_funct(time)
noise = np.random.randn(numPoints)
noisySignal = amplitude + 2*noise

# Visualize our data
plt.figure(figsize=(9, 6))
plt.plot(time, noisySignal, 'r.', label="Noisy Data")
plt.plot(time, amplitude, linewidth=2.5, label="True Signal")
plt.legend(loc="best")
plt.xlabel('Time')
plt.ylabel('Amplitude')
plt.title('Noisy Signal')
plt.minorticks_on()
plt.xlim(0, 12*np.pi)

# Use FFT to transform into frequency domain
fft = np.fft.fft(noisySignal)
freq = np.fft.fftfreq(numPoints)
print "Frequencies returned:", freq
i_freq = np.arange(1, numPoints//2)
pfreq = freq[i_freq]
nfreq = freq[-i_freq]
print "Positive frequencies:", freq[i_freq]

# Plot the PSD
psd = np.abs(fft[i_freq])**2 + np.abs(fft[-i_freq])**2
psd_plot = plt.plot(pfreq*(numPoints), psd, 'k-')
plt.xlabel('Frequency')
plt.ylabel('Power')
plt.xlim(0, 50)

# Extract dominant frequencies, check that the periods match our function
peaks, = np.where(psd > 8e4)
peakFreq = pfreq[peaks]
period = 1/peakFreq * (12*np.pi)/numPoints
print "Period of each dominant frequency", period
