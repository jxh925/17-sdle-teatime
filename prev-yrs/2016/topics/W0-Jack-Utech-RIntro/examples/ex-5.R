library("rvest")
url <- "http://en.wikipedia.org/wiki/List_of_U.S._states_and_territories_by_population"
population <- url %>%
    read_html() %>%
    html_nodes(xpath='//*[@id="mw-content-text"]/table') %>%
    html_table()

population <- population[[2]]

population <- population[, -c(1,2,6)]

View(population)

population <- population [-59, ]

population[, c(2, 3, 4, 5)] <- sapply(population[, c(2, 3, 4, 5)], as.character)

population[, c(2, 3, 4, 5)] <- sapply(population[, c(2, 3, 4, 5)], function(column) {
    as.numeric(lapply(column, function(number) {
        return(gsub(number, pattern = "[,]", replacement = ""))
    }))
})

mean(population$`Population estimate, July 1, 2015`)