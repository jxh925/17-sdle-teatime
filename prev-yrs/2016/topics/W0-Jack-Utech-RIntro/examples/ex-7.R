multiply <- function(x) {
    return(function(y) {
        return(x * y)
    })
}

multiply_6_with <- multiply(6)

multiply_6_with(2)
multiply_6_with(4)

###############################################################################
###############################################################################
###############################################################################

normal_key_value <- function(key, value) {
    return(paste0("--", key, "=", value))
}

key_value_generator <- function(key) {
    return(function(value) {
        return(paste0("--", key, "=", value))
    })
}

options <- list(
    nodes = key_value_generator("nodes"),
    memory = key_value_generator("memory")
)